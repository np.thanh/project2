import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {


  isLoggingIn: boolean = true;
  _username: string = "";
  _password: string = "";


  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    //Check user session
    this.userService.checkSession().subscribe(data => {
      if (data.success == true)
        this.router.navigate(['/profile']);
    });
    
  }
  toggleForm(){
    this.isLoggingIn = !this.isLoggingIn;
  }

}
