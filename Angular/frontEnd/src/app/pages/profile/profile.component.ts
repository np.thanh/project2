import { Component, OnInit, ViewChild } from '@angular/core';
import { UserfeedComponent } from 'src/app/components/userfeed/userfeed.component';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  _userName: string = "";

  constructor(private userService: UserService, private router: Router) { }
  @ViewChild(UserfeedComponent) child;

  ngOnInit(): void {
    //Check user session
    this.userService.checkSession().subscribe(data => {
      if (data.success != true)
        this.router.navigate(['']);
      this._userName = data.object.username;
    });

  }

  refreshFeed(){
    this.child.ngOnChanges();
  }

}
