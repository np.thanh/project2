import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  _username: string = "";
  _password: string = "";
  invalidCredentials: boolean = false;

  @Output()
  toggle: EventEmitter<any> = new EventEmitter();

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    //Check user session
    this.userService.checkSession().subscribe(data => {
      if (data.success == true)
        this.router.navigate(['/profile']);
    });
  }

  login() {
    let user: User = new User(0, this._username, this._password, "", "", "", "");
    this.userService.login(user).subscribe((response)=>{
      if(response.object != null) this.router.navigateByUrl("/profile");
      else this.invalidCredentials = true;
    });
  }

  toggleForm(){
    this.toggle.emit();
  }

}
