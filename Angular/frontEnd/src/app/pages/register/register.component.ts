import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  _username: string = "";
  _password: string = "";
  _firstName: string = "";
  _lastName: string = "";
  _email: string = "";

  @Output()
  toggle: EventEmitter<any> = new EventEmitter();

  constructor(private apiServ: UserService, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    //Check user session
    this.apiServ.checkSession().subscribe(data => {
      if (data.success == true)
        this.router.navigate(['/profile']);
    });
  }

  registerAccount() {
    this.apiServ.registerUser(this._username, this._password, this._firstName, this._lastName, this._email).subscribe(data => {
      let user: User = new User(0, this._username, this._password, "", "", "", "");
      this.userService.login(user).subscribe((response)=>{
        this.router.navigateByUrl("/profile");
      });
    })
  }
  toggleForm(){
    this.toggle.emit();
  }

}
