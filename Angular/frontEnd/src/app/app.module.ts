import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { LandingComponent } from './pages/landing/landing.component';
import { UserfeedComponent } from './components/userfeed/userfeed.component';
import { NewpostComponent } from './components/newpost/newpost.component';
import { PostComponent } from './components/post/post.component';
import { HttpClientModule } from '@angular/common/http';
import { CommentComponent } from './components/comment/comment.component';
import { NewcommentComponent } from './components/newcomment/newcomment.component';
import { UserprofileComponent } from './components/userprofile/userprofile.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { UploadpicsComponent } from './components/uploadpics/uploadpics.component';
import { ForgotpasswordComponent } from './pages/forgotpassword/forgotpassword.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FriendslistComponent } from './components/friendslist/friendslist.component';
import { UserinfoComponent } from './components/userinfo/userinfo.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LandingComponent,
    UserfeedComponent,
    NewpostComponent,
    PostComponent,
    CommentComponent,
    NewcommentComponent,
    UploadpicsComponent,
    UserprofileComponent,
    ProfileComponent,
    ForgotpasswordComponent,
    NavbarComponent,
    FriendslistComponent,
    UserinfoComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
