import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from 'src/app/models/post';
import { ImageService } from 'src/app/services/image.service';
import { PostService } from 'src/app/services/post.service';
import { S3Service } from 'src/app/services/s3.service';

@Component({
  selector: 'app-newpost',
  templateUrl: './newpost.component.html',
  styleUrls: ['./newpost.component.css']
})
export class NewpostComponent implements OnInit {

  @Output()
  refreshFeed: EventEmitter<any> = new EventEmitter();
  newPostBody: string = "";
  selectedFiles: any[] = [];

  imageUrls: any[] = [];

  constructor(private postService: PostService, private router: Router, private activatedRoute: ActivatedRoute, private s3Service:S3Service, private imageService: ImageService) {}
  ngOnInit(){}

  submitPost(){
    if(this.selectedFiles.length > 0){

      this.s3Service.uploadPictures(this.selectedFiles).subscribe((jsonReponse)=>{
        let post: Post = new Post(0, 1, "", "", this.newPostBody, new Date(), [], [], false);
        this.postService.createPost(post).subscribe((newPost)=>{
          let last = jsonReponse.object.length;
          let iteration = 0;
          jsonReponse.object.forEach(newUrl => {
            this.imageService.createImage(newPost.object.id, newUrl).subscribe(()=>{
              iteration++;
              if(last == iteration){
                this.selectedFiles = [];
                this.imageUrls = [];
                this.refreshFeed.emit();
                this.newPostBody = "";
              }
            })
          });
        })
      })
    }else{
      let post: Post = new Post(0, 1, "", "", this.newPostBody, new Date(), [], [], false);
      this.postService.createPost(post).subscribe(()=>{
        this.refreshFeed.emit();
        this.newPostBody = "";
        this.selectedFiles = [];
        this.imageUrls = [];
      })
    }
    
  }

  onFileChanged(event: any) {
    let temp: any[] = event.target.files;
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      Array.from(temp).forEach(file => {
        this.selectedFiles.push(file);
        reader.readAsDataURL(file);
        reader.onload = () => {
        this.imageUrls.push(reader.result); 
        };
      });
    }
  }

}
