import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Like } from 'src/app/models/like';
import { Post } from 'src/app/models/post';
import { LikeService } from 'src/app/services/like.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input()
  post: Post = new Post(0, 0, "", "", "", new Date(), [], [], false);
  @Output()
  updateParent: EventEmitter<any> = new EventEmitter();

  likedBy: string = "";

  constructor(private likeService: LikeService) { }

  ngOnInit(): void {
    this.updateLikeTooltip();
  }

  updateLikeTooltip(){
    this.likedBy = "";
    this.likedBy += "Liked by:\n"
    if(this.post.userLiked) this.likedBy += "You\n";
    if(this.post.likes.length > 0){
      this.post.likes.forEach(like=>{
        this.likedBy += like.firstName + " " + like.lastName + '\n';
      })
     }else{
      if(!this.post.userLiked) this.likedBy = "No likes yet!"
     }
  }

  refreshFeed(){
    this.updateParent.emit();
  }
  

  likePost(){
    this.likeService.likePost(new Like(0, 0, this.post.id, "", "")).subscribe();
    this.post.userLiked = !this.post.userLiked;
    this.updateLikeTooltip();

  }

}
