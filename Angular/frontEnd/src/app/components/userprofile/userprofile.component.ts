import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { UserfeedComponent } from 'src/app/components/userfeed/userfeed.component';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { S3Service } from 'src/app/services/s3.service';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  profileUser: string = "";
  imageUrls: any[] = [];
  selectedFiles: any[] = [];
  _imageUrl: string = "";

  _firstName: string = "";
  _lastName: string = "";
  _email: string = "";
  _display: string = "none";
  userId: number = this.route.snapshot.params["id"];
  isMyProfile: boolean = false;

  constructor(private route: ActivatedRoute, private postService: PostService, private userService: UserService, private router: Router, private s3Service: S3Service) {
    route.params.subscribe(val => {
      this.ngOnInit();
    });
   }
  @ViewChild(UserfeedComponent) child;

  ngOnInit(): void {
    let id: number = this.route.snapshot.params["id"];
    this.postService.getPostsGivenId(id).subscribe(data => {
      this.userId = id;
    })

    this.userService.checkSession().subscribe(data => {
      if (data.success != true) {
        this.router.navigate(['']);
      } else {
        if (data.object.id == id) {
          this._display = "";
          this.isMyProfile = true;
        }else{
          this.isMyProfile = false;
        }
      }
    })

    this.userService.getUserById(id).subscribe(data => {
      this.profileUser = data.object;
    })

  }

  saveProfile(){
    if (this.selectedFiles.length > 0) {
      this.s3Service.uploadPictures(this.selectedFiles).subscribe(data => {
        this._imageUrl = data.object[0];
        this.userService.updateUserPic(this.userId, this._imageUrl).subscribe(res =>{
          this.userService.updateUserInfo(this._firstName, this._lastName, this._email).subscribe((jsonResponse)=>{
              this.router.navigate(["/"])
          });
        });
      })
    } else {
          this.userService.updateUserInfo(this._firstName, this._lastName, this._email).subscribe((jsonResponse)=>{
            this.router.navigate(["/"])

          });
    }
  }

  onFileChanged(event: any) {
    let temp: any[] = event.target.files;
    console.log(event.target.files)
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      Array.from(temp).forEach(file => {
        this.selectedFiles.push(file);
        reader.readAsDataURL(file);
        reader.onload = () => {
        this.imageUrls.push(reader.result); 
        };
      });
    }
  }

}
