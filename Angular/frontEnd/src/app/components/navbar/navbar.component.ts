import { Component, Input, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: User = new User(0, "", "", "", "", "", "");
  _username: string = "";

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.update();
  }
  
  update(){
    this.userService.checkSession().subscribe((jsonReponse)=>{
      console.log(jsonReponse);
      this.user = new User(jsonReponse.object.id, jsonReponse.object.username, jsonReponse.object.password, jsonReponse.object.firstName, jsonReponse.object.lastName, jsonReponse.object.url, jsonReponse.email);
      if(this.user.url == null) this.user.url = 'assets/images/EmptyProfile.png'
      this._username = jsonReponse.object.username;
    })
  }

  logout() {
    this.userService.logoutUser().subscribe(data => {
    });
    this.router.navigate(['']);
  }

}
