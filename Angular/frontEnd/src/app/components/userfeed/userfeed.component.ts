import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { PostComment } from 'src/app/models/postComment';
import { PostService } from 'src/app/services/post.service';
import { PostComponent } from '../post/post.component';
import { CommentService } from 'src/app/services/comment.service';
import { Like } from 'src/app/models/like';
import { LikeService } from 'src/app/services/like.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { ImageService } from 'src/app/services/image.service';

@Component({
  selector: 'app-userfeed',
  templateUrl: './userfeed.component.html',
  styleUrls: ['./userfeed.component.css']
})
export class UserfeedComponent implements OnInit {
  counter: number = 5;
  postList: Post[] = [];
  newPostBody: string = "";
  observer: Subscription = new Subscription();
  @Input()
  userId: number = 0;

  constructor(private postService: PostService, private commentService: CommentService, private likeService: LikeService, private userService: UserService, private router: Router, private imageService: ImageService) { }

  // ngOnChanges(){
  //   console.log("NG ON CHANGES")
  //   this.ngOnInit();
  // }

  ngOnInit(){}

  ngOnChanges(): void {
    //Check user session
    this.userService.checkSession().subscribe(data => {
      if (data.success != true)
        this.router.navigate(['']);
    });

    this.postList = [];

    this.postService.getPostsWithPages(this.userId, this.counter).subscribe(posts => {
     
      posts.object.forEach((post: { id: number; user: { id: number; firstName: string; lastName: string; url: string;}; body: string; date: any;}) => {
        let userLiked: boolean = false;
        let postComments: PostComment[] = [];
        let postLikes: Like[] = [];
        let imageUrls: string[] = [];

         this.commentService.getCommentsByPost(post.id).subscribe(jsonReponse =>{
          jsonReponse.object.forEach((comment: any) => {
            if(comment.user.url == null) comment.user.url = 'assets/images/EmptyProfile.png';
            postComments.push(new PostComment(comment.id, comment.user.id, comment.post.id, comment.user.firstName, comment.user.lastName, comment.body, comment.date, comment.user.url))
            });
            this.imageService.getImageById(post.id).subscribe(imageResponse => {
            imageResponse.object.forEach(imageUrl => {
              imageUrls.push(imageUrl.url)
            });
            this.likeService.getLikesByPost(post.id).subscribe(likeReponse => {  
            likeReponse.object.forEach(like => {
              postLikes.push(new Like(like.id, like.user.id, like.post.id, like.user.firstName, like.user.lastName));
            });
            this.userService.checkSession().subscribe(sessionResponse=>{
              postLikes.forEach(postLike=>{
                if(postLike.userId == sessionResponse.object.id){
                  userLiked = true;
                  postLikes = postLikes.filter((like) => like.userId != sessionResponse.object.id);
                } 
              })
              if(post.user.url == null) post.user.url = 'assets/images/EmptyProfile.png';
              let newPost: Post = new Post(post.id, post.user.id, post.user.firstName, post.user.lastName, post.body, post.date, postComments, postLikes, userLiked, imageUrls, post.user.url)
              this.postList.push(newPost);
              this.postList.sort((a, b)=>{
                return b.id - a.id;
              })
            });
            });
            });
            });

        });

    });

}

  ngOnDestroy() {
    this.observer.unsubscribe();
  }


  getAllPosts(): any{
    return this.postService.getPosts();
  }
  
  increase() {
    this.counter = +this.counter + 5;
    this.ngOnChanges();
  }

}
