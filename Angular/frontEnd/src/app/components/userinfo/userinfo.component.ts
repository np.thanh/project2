import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-userinfo',
  templateUrl: './userinfo.component.html',
  styleUrls: ['./userinfo.component.css']
})
export class UserinfoComponent implements OnInit {

  @Input()
  userId: number = 0;
  user: User = new User(0, "", "", "", "", "", "");

  constructor(private userService: UserService) { }

  ngOnInit(): void {

  }
  ngOnChanges(){
    this.userService.getUserById(this.userId).subscribe((jsonResponse)=>{
      console.log("OBJECT " + jsonResponse.object)
      this.user = new User(jsonResponse.object.id, jsonResponse.object.username, jsonResponse.object.password, jsonResponse.object.firstName, jsonResponse.object.lastName, jsonResponse.object.url, jsonResponse.object.email);
      if(jsonResponse.object.url == null) this.user.url = 'assets/images/EmptyProfile.png';

    })
  }
}
