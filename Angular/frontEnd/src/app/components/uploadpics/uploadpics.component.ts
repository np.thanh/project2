import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uploadpics',
  templateUrl: './uploadpics.component.html',
  styleUrls: ['./uploadpics.component.css']
})
export class UploadpicsComponent implements OnInit {

  public selectedFile: any;
  public event1 : any;
  imgURL: any;
  receivedImageData: any;
  base64Data: any;
  convertedImage: any;

  constructor(private httpCli: HttpClient) { }

  ngOnInit(): void {
  }

  onFileSelected(event: any){
    console.log(event);
    this.selectedFile = event.target.files[0];
  }

  onUpload(){
    const uploadData = new FormData();
    uploadData.append('image', this.selectedFile, this.selectedFile.name);
    this.httpCli.post('http://localhost:9000/api/s3', uploadData)

      .subscribe(
          res => {console.log(res);
          this.receivedImageData = res;
          this.base64Data = this.receivedImageData.pic;
          this.convertedImage = 'data:image/jpeg;base64,' + this.base64Data; },
          err => console.log('Error Occured duringng saving: ' + err)
      );
  }

}
