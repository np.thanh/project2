import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlConstant } from '../globals/url';

@Injectable({
  providedIn: 'root'
})
export class ResetService {

  constructor(private httpClient: HttpClient) { }

  checkEmail(email: string) {
    return this.httpClient.get<any>(UrlConstant.apiURL + `/api/user/checkEmail/${email}`, {withCredentials: true});
    }


    createToken(email: string) {
      return this.httpClient.get<any>(UrlConstant.apiURL + `/api/createToken/${email}`, {withCredentials: true});
    }

    compareToken(token: string, email: string) {
      return this.httpClient.post<any>(UrlConstant.apiURL + `/api/compareToken`, {
        token: token,
        userEmail: email
      }, {withCredentials: true});
    }

    updatePassword(password: string, email: string) {
      return this.httpClient.patch<any>(UrlConstant.apiURL + `/api/user/updatePassword`, {
        email: email,
        password: password
      },{withCredentials: true});
    }
}
