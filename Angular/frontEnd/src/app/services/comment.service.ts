import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlConstant } from '../globals/url';
import { PostComment } from '../models/postComment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {


  constructor(private httpClient: HttpClient) { }
  
  getCommentsByPost(id: number) : Observable<any>{
    return this.httpClient.get(UrlConstant.apiURL + "/api/comment/get-comments-by-postId/" + id, {withCredentials: true});
  }
  createComment(postComment: PostComment) : Observable<any>{
    return this.httpClient.post(UrlConstant.apiURL + "/api/comment/" + postComment.postId, 
    {
      body: postComment.body
    }, 
    {withCredentials: true})
  }
}


