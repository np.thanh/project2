import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlConstant } from '../globals/url';
import { Like } from '../models/like';

@Injectable({
  providedIn: 'root'
})
export class LikeService {



  constructor(private httpClient: HttpClient) { }


  getLikesByPost(postId: number) : Observable<any>{
    return this.httpClient.get(UrlConstant.apiURL + "/api/like/get-likes-by-postId/" + postId, {withCredentials: true});
  }
  likePost(like: Like) : Observable<any>{
    let newLike = 
    {
      post:{
        id: like.postId
      }
    }
    return this.httpClient.post(UrlConstant.apiURL + "/api/like/", newLike, {withCredentials: true});
  }}
