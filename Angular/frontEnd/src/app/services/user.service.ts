import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { UrlConstant } from '../globals/url';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }


  login(user: User) : Observable<any>{
    return this.httpClient.post(UrlConstant.apiURL + "/api/user/login", user, {withCredentials: true});
  }

  checkSession() : Observable<any>{
    return this.httpClient.get(UrlConstant.apiURL + "/api/user/check-session", {withCredentials: true});
  }

  registerUser(username: string, password: string, firstName: string, lastName: string, email: string) {
    return this.httpClient.post<any>(UrlConstant.apiURL + "/api/user", {
      username: username,
      password: password,
      firstName: firstName,
      lastName: lastName,
      email: email,
      url: "https://project2-firebook-bucket.s3.us-east-2.amazonaws.com/EmptyProfile.png"
    }, {withCredentials: true})
  }

  logoutUser() {
    return this.httpClient.get<any>(UrlConstant.apiURL + "/api/user/logout", {withCredentials: true});
  }

  getAllUsers(){
    return this.httpClient.get<any>(UrlConstant.apiURL + "/api/user", {withCredentials: true});
  }

  updateUserInfo(firstName: string, lastName: string, email: string) {
    console.log("updating")
    return this.httpClient.patch<any>(UrlConstant.apiURL + "/api/user", {
      id: "",
      username: "",
      password: "",
      firstName: firstName,
      lastName: lastName,
      email: email,
      url: ""
    }, {withCredentials: true});
  }

  getUserById(id: number) {
    return this.httpClient.get<any>(UrlConstant.apiURL + `/api/user/${id}`, {withCredentials: true});
  }

  updateUserPic(id: number, url: string) {
    console.log(url)
    return this.httpClient.patch<any>(UrlConstant.apiURL + `/api/user`, {
      id: id,
      username: "",
      password: "",
      firstName: "",
      lastName: "",
      email: "",
      url: url
    }, {withCredentials: true})
  }


}
