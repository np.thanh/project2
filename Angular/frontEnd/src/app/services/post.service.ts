import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post';
import { UrlConstant } from '../globals/url';




@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private httpClient: HttpClient) { }


  getPosts() : Observable<any>{
    return this.httpClient.get(UrlConstant.apiURL + "/api/post", {withCredentials: true});
  }

  getPostsGivenId(id: number) :Observable<any>{
    return this.httpClient.get(UrlConstant.apiURL + `/api/post/get-posts-by-userId/${id}?pageNo=0`, {withCredentials: true});
  }

  createPost(post: Post) : Observable<any>{
    
    return this.httpClient.post(UrlConstant.apiURL + "/api/post", 
    {
      body: post.body
    }, 
    {withCredentials: true});
  }

  getPostsWithPages(id: number, page: number) : Observable<any>{
    return this.httpClient.get(UrlConstant.apiURL + `/api/post/get-posts-by-userId/${id}?pageSize=${page}`, {withCredentials: true});
  }

}
