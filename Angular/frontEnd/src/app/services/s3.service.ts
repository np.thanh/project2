import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlConstant } from '../globals/url';

@Injectable({
  providedIn: 'root'
})
export class S3Service {

  constructor(private httpClient: HttpClient) { }



  uploadPicture(selectedImage: any) : Observable<any>{
    const uploadData = new FormData();
    uploadData.append('image', selectedImage, selectedImage.name);
    return this.httpClient.post(UrlConstant.apiURL + + '/api/s3', uploadData);
  }

  uploadPictures(selectedImages: any[]) : Observable<any>{
    let uploadData = new FormData();
    selectedImages.forEach((selectedImage)=>{
      uploadData.append('image', selectedImage, selectedImage.name);
    })
    return this.httpClient.post(UrlConstant.apiURL + '/api/s3', uploadData, {withCredentials: true});
  }
}
