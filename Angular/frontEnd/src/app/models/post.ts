import { Like } from "./like";
import { PostComment } from "./postComment";

export class Post{
    id: number = 0;
    userId: number = 0;
    firstName: string = '';
    lastName: string = '';
    body: string = '';
    date: Date = new Date();
    comments: PostComment[] = []
    likes: Like[] = [];
    userLiked: boolean = false;
    imageUrls?: string[] = [];
    url?: string = '';

    constructor(id:number, userId:number, firstName: string, lastName: string, body:string, date:Date, comments: PostComment[], likes: Like[], userLiked: boolean, imageUrls?: string[], url?: string){
        this.id = id;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.body = body;
        this.date = date;
        this.comments = comments;
        this.likes = likes;
        this.userLiked = userLiked;
        this.imageUrls = imageUrls;
        this.url = url;
    }

}