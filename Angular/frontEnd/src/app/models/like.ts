export class Like{

    id: number = 0;
    userId: number = 0;
    postId: number = 0;
    firstName: String = "";
    lastName: String = "";

    constructor(id: number, userId: number, postId: number, firstName: String, lastName: String){
        this.id = id;
        this.userId = userId;
        this.postId = postId;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}