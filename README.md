----- PROJECT NAME ----

Revature Social Network Platform

---- PROJECT DESCRIPTION ----
In this social network, everyone is friends with everyone. As a user, you will be able to register and login to your account. When you successfully login, you are automatically friends with everyone which means you will see a feed of everyone's posts. Users will be able to create a post and like other people's posts. Users will also have the ability to view other profiles which will display posts of that specific user.

---- TECHNOLOGY USED ----

Java
HTML
CSS
PostGreSQL
AWS RDS
AWS S3
TypeScript
Angular
SpringBoot
JUnit, Mockito, Jasmine

---- FEATURES ----

Users can register/login to the network.
After register, a welcome email will be sent to the email address that provided.
When reset password, an email will be sent with token to reset password.
Users can have friend list, update info, make a post, make a comment, upload picture to the profile or posts.

---- GETTING STARTED ----

The back-end has been deployed and running on the EC2.
For the front-end, open project frontEnd on Angular, the use these command to install and run project: npm install -> ng serve -o
Enjoy the program
