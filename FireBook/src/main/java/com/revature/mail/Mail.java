package com.revature.mail;

import com.revature.model.User;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {
    public void sendMail(String recipient, String token) {
        String to = recipient;

        String from = "reimbursement569@gmail.com";

        String host = "smtp.gmail.com";

        Properties properties = System.getProperties();

        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, "Reimb2468");
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            message.setSubject("Password Reset");

            message.setText("Copy and Paste Token into website \n\nToken: " + token);
            System.out.println("Sending....");

            Transport.send(message);

            System.out.println("sent!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void welcomeMail(User user){
        String to = user.getEmail();

        String from = "firebookapp@gmail.com";

        String host = "smtp.gmail.com";

        Properties properties = System.getProperties();

        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, "FireteamRevature2021");
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            message.setSubject("Welcome to Firebook");

            message.setText("Thank you for signing up " + user.getUsername() + ". Hope you enjoy Firebook.");
            System.out.println("Sending....");

            Transport.send(message);

            System.out.println("sent!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
