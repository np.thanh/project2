package com.revature.repository;


import com.revature.model.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository("likeDao")
@Transactional
public interface LikeDao extends JpaRepository<Like, Integer> {

   @Query("from Like where post.id = :postId")
    List<Like> findLikesByPostId(@Param("postId") Integer postId);

    @Query("from Like where user.id = :userId")
    List<Like> findLikesByUserId(@Param("userId") Integer userId);

    @Query("from Like where user.id = :userId and post.id = :postId")
    Like findLikeByUserIdAndPostId(@Param("userId") Integer userId, @Param("postId") Integer postId);
}
