package com.revature.repository;

import com.revature.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository("imageDao")
@Transactional
public interface ImageDao extends JpaRepository<Image, Integer> {

    @Query("from Image where post.id = :postId")
    List<Image> getImageByPostId(@Param("postId") Integer postId);
}
