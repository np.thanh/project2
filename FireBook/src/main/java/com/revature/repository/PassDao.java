package com.revature.repository;

import com.revature.model.PasswordReset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository("PassDao")
@Transactional
public interface PassDao extends JpaRepository<PasswordReset, Integer> {
    @Query("from PasswordReset where userEmail = :userEmail")
    PasswordReset getTokenByUserId(String userEmail);
}
