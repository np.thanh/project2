package com.revature.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

@Entity
@Table(name="comments")
@Data
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private Integer id;
    @Column(name = "body", length = 10000)
    private String body;
    @Column(name = "date")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd @HH:mm:ss", timezone="EST")
    private Timestamp date;
    @ManyToOne
    private User user;
    @ManyToOne
    private Post post;

    public Comment(String body, User user, Post post){
        this.body = body;
        this.user = user;
        this.post = post;
        this.date = Timestamp.from(Instant.now());
    }


}
