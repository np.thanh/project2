package com.revature.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "passwordReset")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PasswordReset {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pass_id")
    private Integer id;

    @Column(name = "pass_user_id")
    private String userEmail;

    @Column(name = "pass_keyphrase")
    private String token;

    public PasswordReset(String userEmail, String keyPhrase) {
        this.userEmail = userEmail;
        this.token = keyPhrase;
    }
}
