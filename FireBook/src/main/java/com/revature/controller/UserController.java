package com.revature.controller;

import com.revature.model.JsonResponse;
import com.revature.model.User;
import com.revature.service.UserService;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController("userController")
@CrossOrigin(value = "http://localhost:4200", allowCredentials = "true") //this is to fix CORS AND sessions
@RequestMapping(value = "api/user")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @GetMapping
    public JsonResponse getAllUsers(){
        return new JsonResponse(true, "listing all users", this.userService.getAllUsers());
    }

    @PostMapping
    public JsonResponse createUser(@RequestBody User user){
        User tempUser = this.userService.createUser(user);
        if(tempUser != null){
            return new JsonResponse(true, "user has been created", tempUser.getUsername());
        } else{
            return new JsonResponse(false, "Username or email already exists", null);
        }
    }

    @GetMapping("username/{username}")
    public JsonResponse getUserByUsername(@PathVariable String username){
        JsonResponse jsonResponse;
        User user = this.userService.getUserByUsername(username);

        if (user != null){
            jsonResponse = new JsonResponse(true, "user found", user);
        } else{
            jsonResponse = new JsonResponse(false, "user not found", null);
        }
        return jsonResponse;
    }

    @GetMapping("{id}")
    public JsonResponse getUserById(@PathVariable Integer id) {
        return new JsonResponse(true, "user found", this.userService.getUserById(id));
    }

    @GetMapping("checkEmail/{email}")
    public JsonResponse sendEmail(@PathVariable String email) {
        User tempUser = this.userService.getUserByEmail(email);
        if (tempUser == null)
            return new JsonResponse(false, "Incorrect email", null);

        return new JsonResponse(true, "Email found", email);
    }

    @PatchMapping("updatePassword")
    public JsonResponse updatePassword(@RequestBody User user) {
        this.userService.updatePassword(user.getPassword(), user.getEmail());
        return new JsonResponse(true, "password updated", user.getEmail());
    }

    @PatchMapping
    public JsonResponse updateUser(HttpSession session, @RequestBody User user){
        User tempUser = (User) session.getAttribute("loggedInUser");
        user.setId(tempUser.getId());
        return new JsonResponse(true, "User updated", this.userService.updateUser(user));
    }
    @GetMapping("check-session")
    public JsonResponse checkSession(HttpSession session){
        JsonResponse jsonResponse;
        User user = (User) session.getAttribute("loggedInUser");
        if(user != null){
            User tempUser = userService.getUserById(user.getId());
            jsonResponse = new JsonResponse(true, "session found", tempUser);
        }else{
            jsonResponse = new JsonResponse(false, "no session found", null);
        }

        return jsonResponse;
    }

    @PostMapping("login")
    public JsonResponse login(HttpSession session, @RequestBody User user){
        User tempUser = userService.login(user);
        if (tempUser != null){
            session.setAttribute("loggedInUser", tempUser);
            return new JsonResponse(true,"logged in and session created",user);
        } else {
            return new JsonResponse(false, "Wrong username or password", null);
        }

    }

    @GetMapping("logout")
    public JsonResponse logout(HttpSession session){
        session.setAttribute("loggedInUser",null);

        return new JsonResponse(true, "session terminated",null);
    }

}


