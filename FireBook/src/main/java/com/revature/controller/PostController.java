package com.revature.controller;

import com.revature.model.JsonResponse;
import com.revature.model.Post;
import com.revature.model.User;
import com.revature.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@RestController("postController")
@CrossOrigin(value = "http://localhost:4200", allowCredentials = "true") //this is to fix CORS AND sessions
@RequestMapping(value = "api/post")
public class PostController {
    private PostService postService;

    @Autowired
    public PostController(PostService postService){this.postService = postService;}

    @GetMapping("")
    public JsonResponse getAllPosts(){
        return new JsonResponse(true, "View all posts", this.postService.getAllPosts());
    }


    @PostMapping
    public JsonResponse createPost( HttpSession session, @RequestBody Post post){
        User user = (User) session.getAttribute("loggedInUser");
        post.setUser(user);
        post.setDate(Timestamp.from(Instant.now()));
        return new JsonResponse(true, "Post created successful", this.postService.createPost(post));
    }

    //Will return all posts and usernames
    @GetMapping("get-posts-and-username")
    public JsonResponse getPostsAndUsername(){
        return new JsonResponse(true, "List of posts associated with username", this.postService.getPostAndUser());
    }

    //Will return all posts for that specific username(userId)
    @GetMapping("get-posts-by-userId/{userId}")
    public JsonResponse getPostByUserId(@PathVariable Integer userId,
                                        @RequestParam(defaultValue = "0") Integer pageNo,
                                        @RequestParam(defaultValue = "3", required = false) Integer pageSize,
                                        @RequestParam(defaultValue = "date", required = false) String sortBy){
        if (userId == 0) {
            List<Post> posts = this.postService.findPostByUserId(0, pageNo, pageSize, sortBy);
            return new JsonResponse(true, "View all posts", posts);
        }
        JsonResponse jsonResponse;
        List<Post> posts = this.postService.findPostByUserId(userId, pageNo, pageSize, sortBy);
        if(posts != null){
            jsonResponse = new JsonResponse(true, "List posts associate with userId", posts);
        } else {
            jsonResponse = new JsonResponse(false, "Post not found", null);
        }
        return jsonResponse;
    }


}