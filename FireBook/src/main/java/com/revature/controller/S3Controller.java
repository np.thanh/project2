package com.revature.controller;

import com.revature.model.JsonResponse;
import com.revature.service.S3Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RestController("s3Controller")
@CrossOrigin(value = "http://localhost:4200", allowCredentials = "true") //this is to fix CORS AND sessions
@RequestMapping(value = "api/s3")
public class S3Controller {
    S3Services s3Services;

    @Autowired
    public S3Controller(S3Services s3Services) {
        this.s3Services = s3Services;
    }

    @PostMapping("")
    public JsonResponse uploadImage(@RequestParam("image") MultipartFile[] files) throws IOException {

        List<URL> urls = new ArrayList<>();
        for(MultipartFile file : files) {
            s3Services.uploadFile(s3Services.convertMultipartFileToFile(file));
            urls.add(s3Services.getURLFromS3(file));
        }
        return new JsonResponse(true, "Image url", urls);
    }
}
