package com.revature.controller;


import com.revature.model.Comment;
import com.revature.model.JsonResponse;
import com.revature.model.Post;
import com.revature.model.User;
import com.revature.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController("commentController")
@RequestMapping(value = "api/comment")
@CrossOrigin(value = "http://localhost:4200", allowCredentials = "true") //this is to fix CORS AND sessions
//@CrossOrigin(value = "http://3.137.201.92:4200", allowCredentials = "true") //this is to fix CORS AND sessions
public class CommentController {
    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService){this.commentService = commentService;}

    @GetMapping
    public JsonResponse getAllComments(){
        return new JsonResponse(true, "List all comments", this.commentService.getAllComments());
    }

    @PostMapping
    @RequestMapping(value = "{postId}")
    public JsonResponse createComment(HttpSession session, @RequestBody Comment comment, @PathVariable Integer postId){
        User user = (User) session.getAttribute("loggedInUser");
        Comment newComment = new Comment(comment.getBody(), user, new Post(postId));
        return new JsonResponse(true, "comment created", this.commentService.createComment(newComment));
    }

    @GetMapping("get-comments-by-postId/{postId}")
    public JsonResponse getCommentByPostId(@PathVariable Integer postId){
        JsonResponse jsonResponse;
        List<Comment> comments = this.commentService.findCommentsByPostId(postId);
        if(comments != null){
            jsonResponse = new JsonResponse(true, "List of comments", comments);
        } else {
            jsonResponse = new JsonResponse(false, "Comment not found", null);
        }
        return jsonResponse;
    }
}
