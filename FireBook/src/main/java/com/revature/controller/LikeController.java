package com.revature.controller;


import com.revature.model.Comment;
import com.revature.model.JsonResponse;
import com.revature.model.Like;
import com.revature.model.User;
import com.revature.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;

@RestController("likeController")
@RequestMapping(value = "api/like")
@CrossOrigin(value = "http://localhost:4200", allowCredentials = "true") //this is to fix CORS AND sessions
public class LikeController {
    private LikeService likeService;

    @Autowired
    public LikeController(LikeService likeService){this.likeService = likeService;}

    @GetMapping
    public JsonResponse getAllLikes(){
        return new JsonResponse(true, "Lists all likes", this.likeService.getAllLikes());
    }

    @PostMapping
    public JsonResponse toggleLike(HttpSession session, @RequestBody Like like){
        User user = (User) session.getAttribute("loggedInUser");
        Like newLike = new Like(user.getId(), like.getPost().getId());
        return new JsonResponse(true, "You liked the post", this.likeService.toggleLike(newLike));
    }

    @GetMapping("get-likes-by-postId/{postId}")
    public JsonResponse getLikesByPostId(@PathVariable Integer postId){
        JsonResponse jsonResponse;
        List<Like> likes = this.likeService.findLikesByPostId(postId);
        if(likes != null){
            jsonResponse = new JsonResponse(true, "List of likes by postId", likes);
        } else {
            jsonResponse = new JsonResponse(false, "likes not found", null);
        }
        return jsonResponse;
    }

    @GetMapping("get-likes-by-userId/{userId}")
    public JsonResponse getLikesByUserId(@PathVariable Integer userId){
        JsonResponse jsonResponse;
        List<Like> likes = this.likeService.findLikesByUserId(userId);
        if(likes != null){
            jsonResponse = new JsonResponse(true, "List of likes by postId", likes);
        } else {
            jsonResponse = new JsonResponse(false, "likes not found", null);
        }
        return jsonResponse;
    }

}
