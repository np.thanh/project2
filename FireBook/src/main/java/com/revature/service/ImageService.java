package com.revature.service;

import com.revature.model.Image;
import com.revature.repository.ImageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("imageService")
public class ImageService {
    ImageDao imageDao;

    @Autowired
    public ImageService(ImageDao imageDao){this.imageDao = imageDao;}

    public List<Image> getAllImages(){return this.imageDao.findAll();}

    public Image createImage(Image image){return this.imageDao.save(image);}

    public List<Image> getImageByPostId(Integer postId){return this.imageDao.getImageByPostId(postId);}


}
