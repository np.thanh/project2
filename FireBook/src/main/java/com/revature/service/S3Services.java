package com.revature.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

@Service("s3Service")
public class S3Services {
    private String awsID = "AKIAY43QFV6HBQJA6SFQ";
    private String awsKey = "+/eDjiOMAiuj9FrEsywpV2V9Op8Q3t5NWZ/32XV8";
    private String region ="us-east-2";
    private String bucketName = "project2-firebook-bucket";


    BasicAWSCredentials awsCredentials = new BasicAWSCredentials(awsID,awsKey);

    AmazonS3 s3Client = AmazonS3ClientBuilder
            .standard()
            .withRegion(Regions.fromName(region))
            .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
            .build();

    public void uploadFile(File file){

        s3Client.putObject(bucketName, file.getName(),file);
    }

    public File convertMultipartFileToFile(MultipartFile file) throws IOException{
        File convertedFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convertedFile);
        fos.write(file.getBytes());
        fos.close();

        return convertedFile;
    }

    public URL getURLFromS3(MultipartFile file){
        URL s3Url = s3Client.getUrl("project2-firebook-bucket", file.getOriginalFilename());
        return s3Url;
    }


}
