package com.revature.service;

import com.revature.model.PasswordReset;
import com.revature.model.User;
import com.revature.repository.PassDao;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PassService {
    private PassDao passDao;

    @Autowired
    public PassService(PassDao passDao) { this.passDao = passDao; }

    public void createToken(PasswordReset passwordReset) { passDao.save(passwordReset); }

    public PasswordReset getToken(String userEmail) { return passDao.getTokenByUserId(userEmail); }

    public void deleteByEmail(String email) {
        PasswordReset passwordReset = this.passDao.getTokenByUserId(email);
        this.passDao.delete(passwordReset);
    }
}